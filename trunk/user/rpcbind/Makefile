SRC_NAME=rpcbind-1.2.5
SRC_URL=https://downloads.sourceforge.net/rpcbind/$(SRC_NAME).tar.bz2

CFLAGS += -ffunction-sections -fdata-sections -fvisibility=hidden
LDFLAGS += -Wl,--gc-sections

all: download_test extract_test config_test
	$(MAKE) -j$(HOST_NCPU) -C $(SRC_NAME)

download_test:
	( if [ ! -f $(SRC_NAME).tar.bz2 ]; then \
		wget -t5 --timeout=20 --no-check-certificate -O $(SRC_NAME).tar.bz2 $(SRC_URL); \
	fi )

extract_test:
	( if [ ! -d $(SRC_NAME) ]; then \
		tar xjf $(SRC_NAME).tar.bz2; \
	fi )

config_test:
	( if [ -f ./config_done ]; then \
		echo "the same configuration"; \
	else \
		make configure && touch config_done; \
	fi )

configure:
	( cd $(SRC_NAME) ; \
	autoreconf -fi ; \
	PKG_CONFIG_PATH=$(STAGEDIR)/lib/pkgconfig \
	./configure \
		--prefix=/ \
		$(if $(CONFIG_IPV6),--enable-ipv6,--disable-ipv6) \
		--disable-libwrap \
		--disable-debug \
		--disable-warmstarts \
		--with-rpcuser=nobody \
		--without-nss-modules \
		--without-systemdsystemunitdir \
		--host=$(HOST_TARGET) \
		--build=$(HOST_BUILD); \
	)

clean:
	if [ -f $(SRC_NAME)/Makefile ] ; then \
		$(MAKE) -C $(SRC_NAME) distclean ; \
	fi ; \
	rm -rf config_done configs

romfs:
	$(ROMFSINST) $(SRC_NAME)/rpcbind /sbin/rpcbind
